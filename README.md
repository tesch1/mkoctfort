# mkoctfort

This converts the octave mex header prototypes into fortran wrapper functions.

# What now?

This dumps out an octave fortran77 interface (.cmn and .cc).

# TODO

todo: make f90 interface specs too.

# Usage

run like this: `./mkoctfort.py [.../path/to/mexproto.h]` (ie
/usr/include/octave-4.4.1/octave/mexproto.h)

It will write two files: `mex.cmn` and `fort.cc` which can be used to
compile fortran 77 mex files...

mex.cmn can be included in the main fortran file somefunc.F:

```fortran
      subroutine mexFunction(nlhs,plhs,nrhs,prhs)
        include 'mex.cmn'
        integer nlhs,nrhs
        mwPointer plhs(*),prhs(*)
```

then when building, the cc wrapper file can be included along with the
.F file:

```octave
  octave:1> mex somefunc.F fort.cc
```
